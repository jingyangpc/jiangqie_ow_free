
![酱茄企业官网Free](https://ow.jiangqie.com/img/banner.png) 

## 一、小程序介绍

酱茄企业官网小程序，酱茄（[www.jiangqie.com](https://www.jiangqie.com/)）专为中小企业开发的轻量级企业建站小程序（基于uni-app + wordpress），后台操作简单，维护方便，无需过多配置就能搭建一个企业小程序。

微信：**jianbing2011**（加微信可微信群交流）

## 二、功能与特点

特点：多端（微信、QQ、百度、H5）、免费开源、功能后台配置

模块：留言反馈、产品服务、新闻动态、合作伙伴、关于我们等

## 三、源码下载

ps：下载包中含前后端源代码！

(client为uniapp开发的前端；jiangqie-ow-free为后端，是一个wordpress插件)

官网下载：[www.jiangqie.com](https://www.jiangqie.com/)

github下载（优先更新）：https://github.com/longwenjunjie/jiangqie_ow_free

gitee下载：https://gitee.com/longwenjunj/jiangqie_ow_free

![酱茄源码系列](https://ow.jiangqie.com/img/jiangqie.png) 

## 四、安装文档/常见问题

安装文档/常见问题：[www.jiangqie.com/owfree/7713.html](https://www.jiangqie.com/owfree/7713.html)

## 五、更新日志

[www.jiangqie.com/owfree/7715.html](https://www.jiangqie.com/owfree/7715.html)

## 六、二次开发/页面路径

[www.jiangqie.com/owfree/7718.html](https://www.jiangqie.com/owfree/7718.html)

## 七、开源协议

遵循GPL V2.0开源协议发布（协议连接 [www.gnu.org/licenses/gpl-2.0.html](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)）

## 八、免责声明

用户在接受酱茄服务之前，请务必仔细阅读本条款（www.jiangqie.com/mzsm）并同意本声明

## 九、特别鸣谢

我们借助了开源的力量，才得以快速构建出酱茄企业官网小程序，在此特别感谢他们（排名不分先后）：

1. https://cn.wordpress.org

2. https://github.com/DevinVinson/WordPress-Plugin-Boilerplate

3. https://github.com/Codestar/codestar-framework

4. https://github.com/jin-yufeng/mp-html

5. https://gitee.com/liangei/lime-painter
